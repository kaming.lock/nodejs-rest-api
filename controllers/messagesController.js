const Message = require("../model/Message");
const User = require("../model/User");

const getAllMessages = async (req, res) => {
  const msgs = await Message.find();
  if (!msgs) return res.status(204).json({ message: "No messages found." });
  const ids = msgs.map((msg, i) => {
    return msg.authorId;
  });
  const users = await User.find({ _id: ids }).exec();
  const result = msgs.map((msg, i) => {
    const foundUser = users.filter(
      (user) => user._id.toString() == msg.authorId.toString()
    )[0];
    msg._doc = { ...msg._doc, author: foundUser?.username ?? null };
    return msg;
  });
  res.json(msgs);
};

const createNewMessage = async (req, res) => {
  if (!req?.body?.msg) {
    return res.status(400).json({ message: "Message is required." });
  } else if (!req?.body?.authorId) {
    return res.status(400).json({ message: "Author is required." });
  }

  try {
    const result = await Message.create({
      msg: req.body.msg,
      authorId: req.body.authorId,
    });

    res.status(201).json(result);
  } catch (err) {
    console.error(err);
  }
};

const replyToMessage = async (req, res) => {
  if (!req?.body?.msg) {
    return res.status(400).json({ message: "Message is required." });
  } else if (!req?.body?.authorId) {
    return res.status(400).json({ message: "Author is required." });
  } else if (!req?.body?.target) {
    return res.status(400).json({ message: "Target is required." });
  }

  try {
    const result = await Message.create({
      msg: req.body.msg,
      authorId: req.body.authorId,
      replyTo: req.body.target,
    });

    res.status(201).json(result);
  } catch (err) {
    console.error(err);
  }
};

module.exports = {
  getAllMessages,
  createNewMessage,
  replyToMessage,
};
