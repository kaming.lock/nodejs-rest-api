const User = require("../model/User");

const getAllUsers = async (req, res) => {
  const users = await User.find();
  if (!users) return res.status(204).json({ message: "No users found" });
  res.json(users);
};

const deleteUser = async (req, res) => {
  if (!req?.body?.id)
    return res.status(400).json({ message: "User ID required" });
  const user = await User.findOne({ _id: req.body.id }).exec();
  if (!user) {
    return res
      .status(204)
      .json({ message: `User ID ${req.body.id} not found` });
  }
  const result = await user.deleteOne({ _id: req.body.id });
  res.json(result);
};

const getUser = async (req, res) => {
  if (!req?.params?.id)
    return res.status(400).json({ message: "User ID required" });
  const user = await User.findOne({ _id: req.params.id }).exec();
  if (!user) {
    return res
      .status(204)
      .json({ message: `User ID ${req.params.id} not found` });
  }
  res.json(user);
};

const getUserId = async (req, res) => {
  let user = null;
  if (req?.body?.username)
    user = await User.findOne({ username: req.body.username }).exec();
  else {
    const cookies = req.cookies;
    if (!cookies?.jwt) return res.sendStatus(401);
    const refreshToken = cookies.jwt;

    user = await User.findOne({ refreshToken }).exec();
  }
  if (!user) return res.sendStatus(401);
  res.json({ id: user._id });
};

const getUsername = async (req, res) => {
  let user = null;
  if (req?.body?.id) user = await User.findOne({ _id: req.body.id }).exec();
  else {
    const cookies = req.cookies;
    if (!cookies?.jwt) return res.sendStatus(401);
    const refreshToken = cookies.jwt;

    user = await User.findOne({ refreshToken }).exec();
  }
  if (!user) return res.sendStatus(401);
  res.json({ username: user.username });
};

module.exports = {
  getAllUsers,
  deleteUser,
  getUser,
  getUserId,
  getUsername,
};
