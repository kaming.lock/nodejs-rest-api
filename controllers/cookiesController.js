const hasAuthCookie = async (req, res) => {
  console.log(req.cookies);
  const cookies = req.cookies;
  if (!cookies?.jwt)
    return res.status(204).json({ message: "No cookies found." });
  const refreshToken = cookies.jwt;
  res.status(200).json({ message: "OK" });
};

module.exports = {
  hasAuthCookie,
};
