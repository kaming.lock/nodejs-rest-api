const axios = require("axios");

const getOwnedGames = async (req, res) => {
  var props = ["key", "steamid"];
  if (!props.every((prop) => req?.query.hasOwnProperty(prop))) {
    return res.status(400).json({ message: `Parameter missing` });
  }
  if (req.query.hasOwnProperty("format")) {
    if (req.query.format != "json") {
      return res
        .status(204)
        .json({ message: `Format other than JSON not supported` });
    }
  }
  req.query.format = "json";

  var props = ["key", "steamid", "json"];
  var url = "https://api.steampowered.com/IPlayerService/GetOwnedGames/v0001";
  var params = {};
  props.map((prop) => (params[prop] = req.query[prop]));
  url = url + "?" + new URLSearchParams(params).toString();
  axios.get(url).then((data) => {
    res.json(data.data);
  });
};

const getGameDetails = async (req, res) => {
  var props = ["appids"];
  if (!props.every((prop) => req?.query.hasOwnProperty(prop))) {
    return res.status(400).json({ message: `Parameter missing` });
  }
  if (req.query.hasOwnProperty("l")) {
    switch (req.query.l) {
      case "zh_TW":
      case "tc":
        req.query.l = "tchinese";
        break;
      case "zh_CN":
      case "sc":
        req.query.l = "schinese";
        break;
      case "en":
        req.query.l = "english";
        break;
    }
  } else {
    req.query.l = "english";
  }

  var props = ["appids", "l"];
  var url = "https://store.steampowered.com/api/appdetails";
  var params = {};
  props.map((prop) => (params[prop] = req.query[prop]));
  url = url + "?" + new URLSearchParams(params).toString();
  axios.get(url).then((data) => {
    res.json(data.data);
  });
};

module.exports = {
  getOwnedGames,
  getGameDetails,
};
