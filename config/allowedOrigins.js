const allowedOrigins = [
    'https://www.yoursite.com',
    'http://127.0.0.1:5500',
    'http://localhost:3500',
    'http://localhost:3000',
    'https://locker-nodejs-api.herokuapp.com',
    'https://locker-my-blog.herokuapp.com',
    'https://www.lockerhk.com',
    'https://lockerhk.com',
    'http://www.lockerhk.com',
    'http://lockerhk.com'
];

module.exports = allowedOrigins;