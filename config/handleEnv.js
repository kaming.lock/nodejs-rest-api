const fs = require('fs');
const fsPromises = require('fs').promises;
const path = require('path');
const envPath = path.join(__dirname, '..', '.env');

const handleEnv = async () => {
    console.log('Looking for .env file.');
    if (!fs.existsSync(envPath)) {
        console.error('.env file is missing!');
        console.log('Trying to generate .env file now.');
        const accessTokenSecret = require('crypto').randomBytes(64).toString('hex');
        const refreshTokenSecret = require('crypto').randomBytes(64).toString('hex');
        const dbURI = 'mongodb+srv://martinonline:m98043780@cluster0.f2eqw9z.mongodb.net/myblogDB?retryWrites=true&w=majority';
        const content = `ACCESS_TOKEN_SECRET=${accessTokenSecret}\nREFRESH_TOKEN_SECRET=${refreshTokenSecret}\nDATABASE_URI=${dbURI}`;
        try {
            await fsPromises.writeFile(envPath, content);
            console.log('.env file is generated.');
            require('dotenv').config();
        } catch (err) {
            console.error(err);
        }
    }

    console.log(`.env file ${fs.existsSync(envPath) ? 'is' : 'cannot be'} found.`);
    return fs.existsSync(envPath);
}

module.exports = handleEnv;