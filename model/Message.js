const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const Date = Schema.Types.Date;

const messageSchema = new Schema({
  msg: {
    type: String,
    required: true,
  },
  authorId: {
    type: ObjectId,
    ref: "User",
    required: true,
  },
  replyTo: {
    type: ObjectId,
    ref: "Message",
    required: false,
  },
});

module.exports = mongoose.model("Message", messageSchema);
