const express = require("express");
const router = express.Router();
const cookiesController = require("../../controllers/cookiesController");

router.post("/valid", cookiesController.hasAuthCookie);

module.exports = router;
