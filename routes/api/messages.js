const express = require("express");
const router = express.Router();
const messagesController = require("../../controllers/messagesController");
const ROLES_LIST = require("../../config/roles_list");
const verifyRoles = require("../../middleware/verifyRoles");

router
  .route("/act/send")
  .post(
    verifyRoles(ROLES_LIST.Admin, ROLES_LIST.Editor, ROLES_LIST.User),
    messagesController.createNewMessage
  );

router
  .route("/act/reply")
  .post(
    verifyRoles(ROLES_LIST.Admin, ROLES_LIST.Editor, ROLES_LIST.User),
    messagesController.replyToMessage
  );

router.route("/get").get(messagesController.getAllMessages);

module.exports = router;
