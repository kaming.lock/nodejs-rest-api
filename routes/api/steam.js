const express = require("express");
const router = express.Router();
const steamController = require("../../controllers/steamController");

router.route("/getOwnedGames").get(steamController.getOwnedGames);

router.route("/getGameDetails").get(steamController.getGameDetails);

module.exports = router;
